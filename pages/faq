
			<h2>Frequently Asked Questions (FAQ)<img alt="faq icon" src="images/helpbook_64.png" style="float:right; margin:-8px 0 0 0;" /></h2>

			<h4 id="q1">1. What is KDE?</h4>
			<p>
                        KDE is an umbrella brand encompassing technologies
                        created by the KDE community. KDE SC (KDE Software
                        Compilation) is the leading desktop environment for
                        Linux and UNIX platforms. It is comprised of a
                        workspace, a collection of programs and supporting
                        technologies.</p>

			<p>The homepage for the project is at <a
                        href="http://www.kde.org">www.kde.org</a></p>

			<p>An official introduction is found at <a
                        href="http://www.kde.org/whatiskde/">www.kde.org/whatiskde/</a>
			</p>

			<h4 id="q2">2. I can't install KDE because of package dependency problems.</h4>
			<p>
			Typically Debian doesn't have package dependency problems until something messes it up. Dependency problems and broken packages are extremely rare when using a stable Debian release such as Stretch. Testing and Unstable releases are slightly more prone to these problems, as they have not been through the rigorous level of testing and there may be packaging problems in the repository, meaning that (especially with unstable/Sid), KDE may not always be in an installable state. A workaround is to install the kde-core meta package and pick the rest of the needed applications manually.
			</p>
			<p>
			If you have installed backports of some package onto Debian Stable, and your backport package overwrote a stable package which the KDE stable packages depend on, then you may not be able to install KDE on your system, until you remove the backported package. <strong>This is also highly unsupported</strong>
			</p>

			<h4 id="q3">3. I can't compile Qt/KDE applications! I get this: configure error:
			Qt not found</h4>

			<p>A very common problem when compiling KDE programs (or other ones which use
			Qt), is that the configure script, is unable to find where Qt is, and failing
			with a message like this:</p>

			<p><samp>checking for Qt... configure: error: Qt (&gt;= Qt 3.1.0) (headers and
			libraries) not found. Please check your installation!</samp></p>

			<p>This problem can mean different things:</p>

			<ul>
			<li>You don't have the necessary Qt packages installed.</li>
			<li>The packages are installed, but configure is unable to find them.</li>
			</ul>

			<p>First, make sure you have (at least) <strong>libqt3-mt-dev</strong>
            and <strong>qt3-dev-tools</strong>. If configure is successful, but
            during compilation, the build fails because a file
			is missing (for example, qlist.h), this means this software is using obsolete
			headers. Please, report this fact to the developers of this software, and to
			complete the build, install libqt3-compat-headers.</p>

			<p>If configure still fails with the same error, this means is failing to
			find where Qt is installed. You can pass the exact path to the configure script
			this way:</p>

			<p><kbd>--with-qt-dir=/usr/share/qt3</kbd></p>

			<p>Since this is a bit annoying if you are compiling lots of times, you can
			set the environment variable QTDIR, pointing to /usr/share/qt3 (in bash:
			export QTDIR=/usr/share/qt3).</p>

            <p>If you wish to compile Qt4 applications, please install the
            <strong>libqt4-dev</strong> and <strong>qt4-dev-tools</strong>
            packages.</p>

			<h4 id="q4">4. When I remove one application, my whole KDE is deleted!</h4>

			<p>Please refer to the <a href="kde48.html">KDE 4 page</a> for the available testing/unstable
			metapackages.</p>

			<p>If you installed all KDE, for example, running <kbd>apt-get install kde</kbd>,
			you have installed a metapackage, and all its dependencies (kdebase, kdenetwork, kdegraphics, etc.),
			but the metapackage itself is empty.</p>

			<p>If after this, you remove, for example, kfloppy, you will see that two more
			packages will be removed: kdeutils, and kde. This doesn't means that all your
			utils and all your desktop will be removed, just means that you no longer have
			your <em>complete</em> set of utils, and the whole KDE applications.</p>

			<p>Using aptitude it's a bit different. If you installed the kde metapackage
			using aptitude, the rest of packages installed as dependencies of kde, are
			marked as automatically installed. Then, if you remove one of this dependencies
			(for example, kfloppy), it will remove kdeutils and kde, and the rest of
			packages that those packages depend on (because they are marked as
			automatically installed).</p>

			<p>If you don't want those packages removed, mark them as manually installed
			(with aptitude unmarkauto or pressing <kbd>m</kbd> (lowercase) in GUI mode when
			the package name is highlighted).</p>

			<h4 id="q6">6. I can't decrypt some messages with KMail</h4>

			<p>You probably are missing the gnupg-agent package. Read the document about <a
			href="http://userbase.kde.org/KMail/PGP_MIME">using OpenPGP and
			PGP/MIME</a> in the KMail website. Install pinentry-qt4 and gnupg-agent and
			after that, configure GPG as explained in the HOWTO. Restart X to make sure
			<code>/etc/X11/Xsession.d/90gpg-agent</code> is run.</p>

